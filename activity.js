
// 3 ================== single room
db.users.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basi necessities",
	rooms_available: 10,
	isAvailable: false
});



// 4 ================== multiple rooms
db.users.insertOne({
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A simple room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
});


// 4 ================== multiple rooms
db.users.insertOne({
	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
});


// 5 ================== search for a room
db.users.find({name: "double"});


// 6 ================== update the queen room
db.users.updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_available: 0
		}
	}

);	


// 7 ================== delete room
db.users.deleteOne({
	rooms_available: 0
});